# MiniPNG

Library to support Mini-PNG files (.mp), made-up format by my computer-science teacher.

## Requirements

- PHP 7.2+
- [Composer](https://getcomposer.org)

## Run tests

With Gitlab Runner locally (right after cloning)

```shell script
gitlab-runner exec docker test:project
```

With PHPUnit, after installation

```shell script
./vendor/bin/phpunit --configuration phpunit.xml
```

## Installation

```shell script
composer install
```

## Usage

You can see how to use this library in the
[simtrami/minipng-examples](https://gitlab.com/simtrami/minipng-examples) project.

Install it in your project with composer

```shell script
composer require simtrami/minipng
```
