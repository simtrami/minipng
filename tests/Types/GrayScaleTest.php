<?php
declare(strict_types=1);

namespace MiniPng\Tests\Types;


use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Type\GrayScale;
use PHPUnit\Framework\TestCase;

class GrayScaleTest extends TestCase
{
    public function testCreateFromValidFile(): void
    {
        $this->assertInstanceOf(
            GrayScale::class,
            new GrayScale('minipng-samples/other/ok/damier.mp')
        );
    }

    public function testIncorrectDimensions(): void
    {
        $this->expectException(IncorrectDimensionsException::class);
        new GrayScale('minipng-samples/other/nok/8b-incorrect-dimensions.mp');
    }

    public function testIncorrectPixelType(): void
    {
        $this->expectException(InvalidPixelType::class);
        new GrayScale('minipng-samples/other/nok/8b-invalid-pixel-type.mp');
    }

    public function testGetBitmap(): void
    {
        $miniPng = new GrayScale('minipng-samples/other/ok/gray.mp');
        $this->assertEquals(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 64, 64, 64, 64, 0, 0],
                [0, 64, 64, 128, 128, 64, 64, 0],
                [0, 64, 128, 192, 192, 128, 64, 0],
                [0, 64, 128, 192, 192, 128, 64, 0],
                [0, 64, 64, 128, 128, 64, 64, 0],
                [0, 0, 64, 64, 64, 64, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            $miniPng->getBitmap()
        );
    }
}