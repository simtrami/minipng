<?php
declare(strict_types=1);

namespace MiniPng\Tests\Types;


use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Type\BW;
use PHPUnit\Framework\TestCase;

final class BWTest extends TestCase
{
    public function testCreateFromValidFile(): void
    {
        $this->assertInstanceOf(
            BW::class,
            new BW('minipng-samples/bw/ok/A.mp')
        );
    }

    public function testInvalidPixelType(): void
    {
        $this->expectException(InvalidPixelType::class);
        new BW('minipng-samples/bw/nok/invalid-pixel-type.mp');
    }

    public function testGetBitmap(): void
    {
        $miniPng = new BW('minipng-samples/bw/ok/A.mp');
        $this->assertEquals(
            [
                [1, 1, 0, 0, 0, 0, 1, 1],
                [1, 0, 1, 1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
            ],
            $miniPng->getBitmap()
        );
    }

    public function testBrokenDimensions(): void
    {
        $this->expectException(IncorrectDimensionsException::class);
        new BW('minipng-samples/bw/nok/broken-dimensions.mp');
    }

    public function testUnevenDimensions(): void
    {
        $miniPng = new BW('minipng-samples/bw/ok/uneven-dimensions.mp');
        $this->assertEquals(
            [
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            ],
            $miniPng->getBitmap()
        );
    }

    public function testPrintFirstNameInitial(): void
    {
        $miniPng = new BW('minipng-samples/bw/ok/C.mp');
        $this->assertEquals(
            [
                [1, 1, 0, 0, 0],
                [1, 0, 1, 1, 1],
                [0, 1, 1, 1, 1],
                [0, 1, 1, 1, 1],
                [0, 1, 1, 1, 1],
                [1, 0, 1, 1, 1],
                [1, 1, 0, 0, 0],
            ],
            $miniPng->getBitmap()
        );
    }

    public function testSplitBlack(): void
    {
        $mp = new BW('minipng-samples/bw/ok/split-black.mp');
        $this->assertEquals(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            $mp->getBitmap()
        );
    }
}
