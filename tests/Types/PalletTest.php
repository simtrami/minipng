<?php
declare(strict_types=1);

namespace MiniPng\Tests\Types;


use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IndexOutOfPalletRangeException;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Type\Pallet;
use PHPUnit\Framework\TestCase;

class PalletTest extends TestCase
{
    public function testCreateFromValidFile(): void
    {
        $this->assertInstanceOf(
            Pallet::class,
            new Pallet('minipng-samples/other/ok/french-palette.mp')
        );
    }

    public function testIncorrectDimensions(): void
    {
        $this->expectException(IncorrectDimensionsException::class);
        new Pallet('minipng-samples/other/nok/pallet-incorrect-dimensions.mp');
    }

    public function testIncorrectPixelType(): void
    {
        $this->expectException(InvalidPixelType::class);
        new Pallet('minipng-samples/other/nok/pallet-invalid-pixel-type.mp');
    }

    public function testIndexOutOfPalletRange(): void
    {
        $this->expectException(IndexOutOfPalletRangeException::class);
        $this->expectExceptionMessage("The index 2 is not in pallet's range (0-1)");
        new Pallet('minipng-samples/other/nok/german-flag-error-palette.mp');
    }

    public function testGetBitmap(): void
    {
        $miniPng = new Pallet('minipng-samples/other/ok/french-palette.mp');
        $this->assertEquals(
            [
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
            ],
            $miniPng->getBitmap()
        );
    }
}