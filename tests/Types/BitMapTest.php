<?php
declare(strict_types=1);

namespace MiniPng\Tests\Types;


use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Type\BitMap;
use PHPUnit\Framework\TestCase;

class BitMapTest extends TestCase
{
    public function testCreateFromValidFile(): void
    {
        $this->assertInstanceOf(
            BitMap::class,
            new BitMap('minipng-samples/other/ok/french-flag.mp')
        );
    }

    public function testIncorrectDimensions(): void
    {
        $this->expectException(IncorrectDimensionsException::class);
        new BitMap('minipng-samples/other/nok/24b-incorrect-dimensions.mp');
    }

    public function testIncorrectPixelType(): void
    {
        $this->expectException(InvalidPixelType::class);
        new BitMap('minipng-samples/other/nok/24b-invalid-pixel-type.mp');
    }

    public function testGetBitmap(): void
    {
        $miniPng = new BitMap('minipng-samples/other/ok/french-flag.mp');
        $this->assertEquals(
            [
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
                [[0, 0, 255], [0, 0, 255], [255, 255, 255], [255, 255, 255], [255, 0, 0], [255, 0, 0]],
            ],
            $miniPng->getBitmap()
        );
    }
}