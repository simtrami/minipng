<?php
declare(strict_types=1);

namespace MiniPng\Tests;


use Exception;
use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;
use MiniPng\MiniPng;
use PHPUnit\Framework\TestCase;

final class MiniPngTest extends TestCase
{
    public function testCreateFromValidFile(): void
    {
        $this->assertInstanceOf(
            MiniPng::class,
            new MiniPng('minipng-samples/bw/ok/A.mp')
        );
    }

    public function testNonExistingFile(): void
    {
        $this->expectException(FileNotFoundException::class);
        new MiniPng('minipng-samples/bw/nok/unicorn.mp');
    }

    public function testNonRegularFile(): void
    {
        $this->expectException(NonRegularFileException::class);
        new MiniPng('minipng-samples/bw/nok');
    }

    /**
     * @throws Exception
     */
    public function testNonReadableFile(): void
    {
        $this->expectException(NonReadableFileException::class);
        $f = fopen('password', 'wb+');
        fwrite($f, "I'm not readable");
        fclose($f);
        chmod('password', 0200);
        try {
            new MiniPng('password');
        } catch (Exception $e) {
            unlink('password');
            throw $e;
        }
    }

    public function testInvalidMagicNumber(): void
    {
        $this->expectException(IncorrectMagicNumberException::class);
        new MiniPng('minipng-samples/bw/nok/wrong-magic.mp');
    }

    public function testGetHeader(): void
    {
        $miniPng = new MiniPng('minipng-samples/bw/ok/A.mp');
        $this->assertEquals(
            ['length' => 8, 'height' => 10, 'pixelType' => 0],
            $miniPng->getHeader()
        );
    }

    public function testMissingHeader(): void
    {
        $this->expectException(MissingBlockException::class);
        new MiniPng('minipng-samples/bw/nok/missing-header.mp');
    }

    public function testIncorrectHeaderContentLengthDefinition(): void
    {
        $this->expectException(IncorrectHeaderException::class);
        new MiniPng('minipng-samples/bw/nok/incorrect-header-content-length-definition.mp');
    }

    public function testMissingHeaderWithHInFile(): void
    {
        $this->expectException(MissingBlockException::class);
        new MiniPng('minipng-samples/bw/nok/missing-header-with-H.mp');
    }

    public function testIncorrectLength(): void
    {
        $this->expectException(IncorrectDimensionsException::class);
        new MiniPng('minipng-samples/bw/nok/incorrect-length.mp');
    }

    public function testIncorrectHeight(): void
    {
        $this->expectException(IncorrectDimensionsException::class);
        new MiniPng('minipng-samples/bw/nok/incorrect-height.mp');
    }

    public function testPixelTypeOutOfRange(): void
    {
        $this->expectException(InvalidPixelType::class);
        new MiniPng('minipng-samples/bw/nok/pixel-type-out-of-range.mp');
    }

    /**
     * @throws Exception
     */
    public function testGetComment(): void
    {
        $miniPng = new MiniPng('minipng-samples/bw/ok/A.mp');
        try {
            $this->assertEquals(
                'La lettre A',
                $miniPng->getComment()
            );
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function testNoComment(): void
    {
        $miniPng = new MiniPng('minipng-samples/bw/ok/no-comment.mp');
        $this->assertEquals(
            null,
            $miniPng->getComment()
        );
    }

    public function testIncorrectLengthForCommentAsLastBlock(): void
    {
        $this->expectException(ChunkOutOfRangeException::class);
        new MiniPng('minipng-samples/bw/nok/incorrect-last-block-length2.mp');
    }

    public function testMissingData(): void
    {
        $this->expectException(MissingBlockException::class);
        new MiniPng('minipng-samples/bw/nok/missing-data.mp');
    }

    public function testSplitBlack(): void
    {
        $mp = new MiniPng('minipng-samples/bw/ok/split-black.mp');
        $this->assertEquals(
            "Exemple d'une image en noir et blanc...... dont les donnees sont eclatees en morceaux",
            $mp->getComment()
        );
    }

    public function testIncorrectLengthForDataAsLastBlock(): void
    {
        $this->expectException(ChunkOutOfRangeException::class);
        new MiniPng('minipng-samples/bw/nok/incorrect-last-block-length.mp');
    }

    public function testMissingPallet(): void
    {
        $this->expectException(MissingBlockException::class);
        $this->expectExceptionMessage('Missing Pallet block');
        new MiniPng('minipng-samples/other/nok/missing-pallet-block.mp');
    }

    public function testInvalidPalletSize(): void
    {
        $this->expectException(IncorrectPalletException::class);
        $this->expectExceptionMessage('The pallet size is invalid');
        new MiniPng('minipng-samples/other/nok/invalid-pallet-size.mp');
    }

    public function testOffsetBlock(): void
    {
        $this->expectException(InvalidBlockDefinitionType::class);
        $this->expectExceptionMessage('Expected new block at offset 22');
        new MiniPng('minipng-samples/bw/nok/offset-block.mp');
    }

    public function testOffsetEndOfFile(): void
    {
        $this->expectException(InvalidBlockDefinitionType::class);
        $this->expectExceptionMessage('Not enough space for a new block starting at offset 37');
        new MiniPng('minipng-samples/bw/nok/offset-end-of-file.mp');
    }
}
