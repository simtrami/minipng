<?php
declare(strict_types=1);

namespace MiniPng\Tests\Utilities;

use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;
use MiniPng\Type\BitMap;
use MiniPng\Type\BW;
use MiniPng\Type\GrayScale;
use MiniPng\Type\Pallet;
use MiniPng\Utility\Create;
use PHPUnit\Framework\TestCase;

final class CreateTest extends TestCase
{
    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     */
    public function testUtilsMiniPng1(): void
    {
        $this->assertInstanceOf(
            BW::class,
            Create::fromFile('minipng-samples/bw/ok/A.mp')
        );
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws IncorrectPalletException
     */
    public function testUtilsMiniPng8(): void
    {
        $this->assertInstanceOf(
            GrayScale::class,
            Create::fromFile('minipng-samples/other/ok/damier.mp')
        );
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws IncorrectPalletException
     */
    public function testUtilsMiniPng24(): void
    {
        $this->assertInstanceOf(
            BitMap::class,
            Create::fromFile('minipng-samples/other/ok/french-flag.mp')
        );
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     */
    public function testUtilsMiniPngPallet(): void
    {
        $this->assertInstanceOf(
            Pallet::class,
            Create::fromFile('minipng-samples/other/ok/french-palette.mp')
        );
    }
}
