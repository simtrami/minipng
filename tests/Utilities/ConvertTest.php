<?php
declare(strict_types=1);

namespace MiniPng\Tests\Utilities;

use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;
use MiniPng\Exception\TargetFileException;
use MiniPng\Utility\Convert;
use PHPUnit\Framework\TestCase;

final class ConvertTest extends TestCase
{
    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testA(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/A.mp', 'A.pbm');

        $this->assertEquals(
            file('pnm-samples/A.pbm'),
            file('A.pbm')
        );

        unlink('A.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testBlack(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/black.mp', 'black.pbm');

        $this->assertEquals(
            file('pnm-samples/black.pbm'),
            file('black.pbm')
        );

        unlink('black.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testC(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/C.mp', 'C.pbm');

        $this->assertEquals(
            file('pnm-samples/C.pbm'),
            file('C.pbm')
        );

        unlink('C.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testNoComment(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/no-comment.mp', 'no-comment.pbm');

        $this->assertEquals(
            file('pnm-samples/no-comment.pbm'),
            file('no-comment.pbm')
        );

        unlink('no-comment.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testSplitBlack(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/split-black.mp', 'split-black.pbm');

        $this->assertEquals(
            file('pnm-samples/split-black.pbm'),
            file('split-black.pbm')
        );

        unlink('split-black.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testUnevenDimensions(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/uneven-dimensions.mp', 'uneven-dimensions.pbm');

        $this->assertEquals(
            file('pnm-samples/uneven-dimensions.pbm'),
            file('uneven-dimensions.pbm')
        );

        unlink('uneven-dimensions.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testUnorderedA(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/bw/ok/unordered_A.mp', 'unordered_A.pbm');

        $this->assertEquals(
            file('pnm-samples/unordered_A.pbm'),
            file('unordered_A.pbm')
        );

        unlink('unordered_A.pbm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testDamier(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/other/ok/damier.mp', 'damier.pgm');

        $this->assertEquals(
            file('pnm-samples/damier.pgm'),
            file('damier.pgm')
        );

        unlink('damier.pgm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testGray(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/other/ok/gray.mp', 'gray.pgm');

        $this->assertEquals(
            file('pnm-samples/gray.pgm'),
            file('gray.pgm')
        );

        unlink('gray.pgm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testFrenchFlag(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/other/ok/french-flag.mp', 'french-flag.ppm');

        $this->assertEquals(
            file('pnm-samples/french-flag.ppm'),
            file('french-flag.ppm')
        );

        unlink('french-flag.ppm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testFrenchFlagPallet(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/other/ok/french-palette.mp', 'french-palette.ppm');

        $this->assertEquals(
            file('pnm-samples/french-palette.ppm'),
            file('french-palette.ppm')
        );

        unlink('french-palette.ppm');
    }

    /**
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function testGermanFlagPallet(): void
    {
        $c = new Convert();
        $c->to_PNM('minipng-samples/other/ok/german-flag-palette.mp', 'german-flag-palette.ppm');

        $this->assertEquals(
            file('pnm-samples/german-flag-palette.ppm'),
            file('german-flag-palette.ppm')
        );

        unlink('german-flag-palette.ppm');
    }
}
