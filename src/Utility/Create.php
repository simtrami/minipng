<?php


namespace MiniPng\Utility;


use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;
use MiniPng\MiniPng;
use MiniPng\Type\BitMap;
use MiniPng\Type\BW;
use MiniPng\Type\GrayScale;
use MiniPng\Type\Pallet;
use RuntimeException;

class Create
{
    /**
     * @param $path
     * @return MiniPng|BW|BitMap|GrayScale|Pallet
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     */
    public static function fromFile($path)
    {
        $mp = new MiniPng($path);
        $pt = $mp->getHeader()['pixelType'];
        switch ($pt) {
            case 0:
                $mp = new BW($path);
                break;
            case 1:
                $mp = new GrayScale($path);
                break;
            case 2:
                $mp = new Pallet($path);
                break;
            case 3:
                $mp = new BitMap($path);
                break;
            default:
                throw new RuntimeException('Fatal error.');
                break;
        }

        return $mp;
    }
}