<?php


namespace MiniPng\Utility;


use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;
use MiniPng\Exception\TargetFileException;
use MiniPng\Type\BitMap;
use MiniPng\Type\BW;
use MiniPng\Type\GrayScale;
use MiniPng\Type\Pallet;
use MiniPng\Type\Type;
use RuntimeException;

class Convert
{
    /**
     * @var string
     */
    private $magic;
    /**
     * @var string
     */
    private $comment;
    /**
     * @var int
     */
    private $length;
    /**
     * @var int
     */
    private $height;
    /**
     * @var int
     */
    private $max;
    /**
     * @var string
     */
    private $data;

    /**
     * @param string $path
     * @param string $target
     * @return int
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws TargetFileException
     */
    public function to_PNM(string $path, string $target): int
    {
        if (!is_dir($target) && (is_writable($target) || !file_exists($target))) {
            $mp = Create::fromFile($path);
            if ($mp instanceof BW) {
                $this->to_PBM($mp);
            } elseif ($mp instanceof GrayScale) {
                $this->to_PGM($mp);
            } elseif ($mp instanceof BitMap || $mp instanceof Pallet) {
                $this->to_PPM($mp);
            } else {
                throw new RuntimeException('Fatal error.');
            }

            $this->create_file($target);
            return 0;
        }
        throw new TargetFileException('The target file is either a directory or is not writeable');
    }

    /**
     * @param BW $mp
     */
    private function to_PBM(BW $mp): void
    {
        $this->set_meta($mp, 'P4');

        $bits = '';
        foreach ($mp->getBitmap() as $ln) {
            foreach ($ln as $b) {
                // MiniPNG sees 0 as black and 1 as white, PBM does the opposite
                $bits .= abs(1 - $b);
            }
            // Fill last byte with zeros if length is not a multiple of 8
            if ($this->length % 8 !== 0) {
                $bits .= str_repeat('0', 8 - $this->length % 8);
            }
        }
        // Convert bytes' string representation into binary
        $this->data = implode(array_map('chr',
            array_map('bindec',
                str_split($bits, 8)
            )
        ));
    }

    /**
     * @param Type $mp
     * @param string $magic
     * @param int|null $max
     * @param int|null $length
     * @param int|null $height
     * @param string|null $comment
     */
    private function set_meta(Type $mp, string $magic, int $max = null, int $length = null, int $height = null, string $comment = null): void
    {
        $this->magic = $magic;
        if ($max) {
            $this->max = $max;
        }
        if ($length) {
            $this->length = $length;
        } else {
            $this->length = $mp->getHeader()['length'];
        }
        if ($height) {
            $this->height = $height;
        } else {
            $this->height = $mp->getHeader()['height'];
        }
        if ($comment) {
            $this->comment = $comment;
        } else {
            $this->comment = $mp->getComment();
        }
    }

    /**
     * @param GrayScale $mp
     */
    private function to_PGM(GrayScale $mp): void
    {
        $this->set_meta($mp, 'P5', 255);

        $noLines = [];
        foreach ($mp->getBitmap() as $line) {
            foreach ($line as $px) {
                $noLines[] = $px;
            }
        }

        $this->data = implode(array_map('chr', $noLines));
    }

    /**
     * @param BitMap|Pallet $mp
     */
    private function to_PPM($mp): void
    {
        $this->set_meta($mp, 'P6', 255);

        $noLines = [];
        foreach ($mp->getBitmap() as $line) {
            foreach ($line as $px) {
                foreach ($px as $hue) {
                    $noLines[] = $hue;
                }
            }
        }

        $this->data = implode(array_map('chr', $noLines));
    }

    /**
     * @param string $target
     */
    private function create_file(string $target): void
    {
        $f = fopen($target, 'wb');
        fwrite($f, $this->magic . "\n");
        if ($this->comment) {
            fwrite($f, '# ' . $this->comment . "\n");
        }
        fwrite($f, $this->length . ' ' . $this->height . "\n");
        if ($this->max) {
            fwrite($f, $this->max . "\n");
        }
        fwrite($f, $this->data);
        fclose($f);
    }
}