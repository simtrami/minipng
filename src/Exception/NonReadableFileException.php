<?php


namespace MiniPng\Exception;


use Exception;

class NonReadableFileException extends Exception
{
}