<?php


namespace MiniPng\Type;


use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;
use MiniPng\MiniPng;

abstract class Type extends MiniPng
{
    /**
     * @var array
     */
    protected $bitmap;

    /**
     * MiniPngBaseType constructor.
     * @param $path
     * @throws IncorrectDimensionsException
     * @throws InvalidPixelType
     * @throws ChunkOutOfRangeException
     * @throws FileNotFoundException
     * @throws IncorrectHeaderException
     * @throws IncorrectMagicNumberException
     * @throws IncorrectPalletException
     * @throws InvalidBlockDefinitionType
     * @throws MissingBlockException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     */
    public function __construct($path)
    {
        parent::__construct($path);
        $pixelType = $this->header['pixelType'];
        if ($this->correct_pixel_type($pixelType)) {
            $this->make_image();
            return;
        }

        throw new InvalidPixelType("The pixel type value ${pixelType} does not match the class encoding");
    }

    /**
     * @param int $pixelType
     * @return bool
     */
    abstract protected function correct_pixel_type(int $pixelType): bool;

    /**
     * @throws IncorrectDimensionsException
     * @throws InvalidPixelType
     */
    abstract protected function make_image(): void;

    /**
     * @return array
     */
    public function getBitmap(): array
    {
        return $this->bitmap;
    }
}