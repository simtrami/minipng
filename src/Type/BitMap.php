<?php

namespace MiniPng\Type;

use MiniPng\Exception\IncorrectDimensionsException;

class BitMap extends Type
{
    /**
     * @inheritDoc
     */
    protected function correct_pixel_type(int $pixelType): bool
    {
        return $pixelType === 3;
    }

    /**
     * @inheritDoc
     */
    protected function make_image(): void
    {
        $expectedLength = $this->header['length'] * $this->header['height'] * 24;
        if (strlen($this->data) === $expectedLength) {
            $this->construct_bitmap();
        } else {
            // Incorrect dimensions
            throw new IncorrectDimensionsException("The data size does not match header's dimensions");
        }
    }

    /**
     * @param string $pixelBits
     * @return array
     */
    private function create_pixel(string $pixelBits): array
    {
        $pixel = [];
        foreach (str_split($pixelBits, 8) as $hue) {
            $pixel[] = bindec($hue);
        }
        return $pixel;
    }

    /**
     *
     */
    private function construct_bitmap(): void
    {
        $lines = str_split($this->data, $this->header['length'] * 24);
        // Split each line in pixels of 24 bits each
        foreach ($lines as $line) {
            $linePixels = [];
            foreach (str_split($line, 24) as $pixelBits) {
                $linePixels[] = $this->create_pixel($pixelBits);
            }
            $this->bitmap[] = $linePixels;
        }
    }
}