<?php

namespace MiniPng\Type;

use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IndexOutOfPalletRangeException;

class Pallet extends Type
{
    /**
     * @inheritDoc
     */
    protected function correct_pixel_type(int $pixelType): bool
    {
        return $pixelType === 2;
    }

    /**
     * @inheritDoc
     * @throws IndexOutOfPalletRangeException
     */
    protected function make_image(): void
    {
        $expectedLength = $this->header['length'] * $this->header['height'] * 8;
        if (strlen($this->data) === $expectedLength) {
            $this->construct_bitmap();
        } else {
            // Incorrect dimensions
            throw new IncorrectDimensionsException("The data size does not match header's dimensions");
        }
    }

    /**
     * @throws IndexOutOfPalletRangeException
     */
    private function construct_bitmap(): void
    {
        $lines = str_split($this->data, $this->header['length'] * 8);
        $palletSize = count($this->pallet);
        // Split each line in pixels of 24 bits each
        foreach ($lines as $line) {
            $linePixels = [];
            foreach (str_split($line, 8) as $index) {
                $i = bindec($index);
                if (0 > $i || $i >= $palletSize) {
                    throw new IndexOutOfPalletRangeException("The index ${i} is not in pallet's range (0-" . (string)($palletSize - 1) . ')');
                }
                $linePixels[] = $this->pallet[$i];
            }
            $this->bitmap[] = $linePixels;
        }
    }
}