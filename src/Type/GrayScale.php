<?php

namespace MiniPng\Type;

use MiniPng\Exception\IncorrectDimensionsException;

class GrayScale extends Type
{
    /**
     * @inheritDoc
     */
    protected function correct_pixel_type(int $pixelType): bool
    {
        return $pixelType === 1;
    }

    /**
     * @inheritDoc
     */
    protected function make_image(): void
    {
        $expectedLength = $this->header['length'] * $this->header['height'] * 8;
        if (strlen($this->data) === $expectedLength) {
            $this->construct_bitmap();
        } else {
            // Incorrect dimensions
            throw new IncorrectDimensionsException("The data size does not match header's dimensions");
        }
    }

    /**
     *
     */
    private function construct_bitmap(): void
    {
        $lines = str_split($this->data, $this->header['length'] * 8);
        // Split each line in pixels of 8 bits each
        foreach ($lines as $line) {
            $linePixels = [];
            foreach (str_split($line, 8) as $pixelBits) {
                $linePixels[] = bindec($pixelBits);
            }
            $this->bitmap[] = $linePixels;
        }
    }
}