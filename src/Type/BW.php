<?php

namespace MiniPng\Type;

use MiniPng\Exception\IncorrectDimensionsException;

class BW extends Type
{
    /**
     * @inheritDoc
     */
    protected function correct_pixel_type(int $pixelType): bool
    {
        return $pixelType === 0;
    }

    /**
     * @inheritDoc
     */
    protected function make_image(): void
    {
        $expectedLength = $this->header['length'] * $this->header['height'];
        if (strlen($this->data) === $expectedLength) {
            $this->construct_bitmap();
        } elseif ($this->is_uneven($expectedLength)) {
            // The dimensions are uneven
            $this->construct_bitmap(true);
        } else {
            // Incorrect dimensions
            throw new IncorrectDimensionsException("The data size does not match header's dimensions");
        }
    }

    /**
     * @param bool $uneven
     */
    private function construct_bitmap(bool $uneven = false): void
    {
        if ($uneven) {
            // The jamming bits are removed
            $lines = array_slice(
                str_split($this->data, $this->header['length']),
                0, $this->header['height']);
        } else {
            $lines = str_split($this->data, $this->header['length']);
        }
        // Split each line in pixels of 1 bit each
        foreach ($lines as $line) {
            $this->bitmap[] = str_split($line, 1);
        }
    }

    /**
     * @param $expectedLength
     * @return bool
     */
    private function is_uneven($expectedLength): bool
    {
        return strlen($this->data) - $expectedLength === 8 - ($expectedLength % 8);
    }
}