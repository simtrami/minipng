<?php

namespace MiniPng;

use MiniPng\Exception\ChunkOutOfRangeException;
use MiniPng\Exception\FileNotFoundException;
use MiniPng\Exception\IncorrectDimensionsException;
use MiniPng\Exception\IncorrectHeaderException;
use MiniPng\Exception\IncorrectMagicNumberException;
use MiniPng\Exception\IncorrectPalletException;
use MiniPng\Exception\InvalidBlockDefinitionType;
use MiniPng\Exception\InvalidPixelType;
use MiniPng\Exception\MissingBlockException;
use MiniPng\Exception\NonReadableFileException;
use MiniPng\Exception\NonRegularFileException;

class MiniPng
{
    /**
     * @var string
     */
    private $content;
    /**
     * @var array
     */
    private $chars;
    /**
     * @var array
     */
    protected $header;
    /**
     * @var string
     */
    private $comment;
    /**
     * @var string
     */
    protected $data;
    /**
     * @var array
     */
    protected $pallet;

    /**
     * Create constructor.
     * @param string $path
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws FileNotFoundException
     * @throws IncorrectMagicNumberException
     * @throws NonReadableFileException
     * @throws NonRegularFileException
     * @throws ChunkOutOfRangeException
     * @throws IncorrectPalletException
     */
    public function __construct($path)
    {
        if (file_exists($path)) {
            if (is_file($path)) {
                if (is_readable($path)) {
                    $file = fopen($path, 'rb');
                    $content = fread($file, filesize($path));
                    fclose($file);
                    if ($this->is_minipng($content)) {
                        $this->read_file($content);
                        return;
                    }
                    throw new IncorrectMagicNumberException("The file ${path} is not of Mini-PNG format");
                }
                throw new NonReadableFileException("The file ${path} is not readable");
            }
            throw new NonRegularFileException("The file ${path} is not a regular file");
        }
        throw new FileNotFoundException("The file ${path} does not exist");
    }

    /**
     * @param false|string $content
     * @return bool
     */
    private function is_minipng($content): bool
    {
        return strpos($content, 'Mini-PNG') === 0;
    }

    /**
     * @param string $content
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws ChunkOutOfRangeException
     * @throws IncorrectPalletException
     */
    private function read_file(string $content): void
    {
        $chars = str_split($content);
        $blockLetters = array_intersect($chars, ['H', 'D']);
        if (in_array('H', $blockLetters, true) && in_array('D', $blockLetters, true)) {
            // The file may be correct
            $this->content = $content;
            $this->chars = $chars;
            $this->parse_content(8);
        } else {
            // The file is not correct
            throw new MissingBlockException('Missing Header or Data block(s)');
        }
    }

    /**
     * @param int $pos
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws InvalidBlockDefinitionType
     * @throws InvalidPixelType
     * @throws MissingBlockException
     * @throws ChunkOutOfRangeException
     * @throws IncorrectPalletException
     */
    private function parse_content(int $pos): void
    {
        if ($this->is_end_of_file($pos)) {
            if ($this->data && $this->header) {
                if ($this->pallet === null && $this->header['pixelType'] === 2) {
                    throw new MissingBlockException('Missing Pallet block');
                }
                return;
            }
            // Missing mandatory block
            throw new MissingBlockException('Missing Header or Data block(s)');

        }

        if (strlen($this->content) >= $pos + 5) {
            // Enough bytes for block id + block length
            switch ($this->chars[$pos]) {
                case 'H':
                    $newPos = $this->read_header($pos + 1);
                    break;
                case 'C':
                    $newPos = $this->read_comment($pos + 1);
                    break;
                case 'D':
                    $newPos = $this->read_data($pos + 1);
                    break;
                case 'P':
                    $newPos = $this->read_pallet($pos + 1);
                    break;
                default:
                    // Invalid block size or file
                    throw new InvalidBlockDefinitionType("Expected new block at offset ${pos}");
            }
            $this->parse_content($newPos);
        } else {
            // Invalid file size
            throw new InvalidBlockDefinitionType("Not enough space for a new block starting at offset ${pos}");
        }
    }

    private function is_end_of_file(int $pos): bool
    {
        return strlen($this->content) === $pos;
    }

    //=======================================
    // READING FUNCTIONS
    //=======================================

    /**
     * @param int $start
     * @param int $length
     * @return string
     * @throws ChunkOutOfRangeException
     */
    private function read_chunk(int $start, int $length): string
    {
        if ($start + $length <= strlen($this->content)) {
            return substr($this->content, $start, $length);
        }
        throw new ChunkOutOfRangeException("Attempted to read a chunk out of the content's range");
    }

    /**
     * @param int $start
     * @param int $length
     * @return int
     * @throws ChunkOutOfRangeException
     */
    private function read_chunk2int(int $start, int $length): int
    {
        return intval(bin2hex($this->read_chunk($start, $length)), 16);
    }

    /**
     * @param int $start
     * @param int $length
     * @return string
     * @throws ChunkOutOfRangeException
     */
    private function read_chunk2bin(int $start, int $length): string
    {
        if ($start + $length <= strlen($this->content)) {
            $binWord = '';
            for ($i = $start; $i < $start + $length; $i++) {
                $byte = base_convert(bin2hex($this->content[$i]), 16, 2);
                if (strlen($byte) < 8) {
                    $byte = str_repeat('0', 8 - strlen($byte)) . $byte;
                }
                $binWord .= $byte;
            }
            return $binWord;
        }
        throw new ChunkOutOfRangeException("Attempted to read a chunk out of the content's range");
    }

    //=======================================
    // HEADER FUNCTIONS
    //=======================================

    /**
     * @param int $pos
     * @return bool
     * @throws ChunkOutOfRangeException
     */
    private function has_correct_header(int $pos): bool
    {
        return $this->read_chunk($pos, 4) === "\x00\x00\x00\x09";
    }

    /**
     * @param int $length
     * @throws IncorrectDimensionsException
     */
    private function set_length(int $length): void
    {
        if ($length > 0) {
            $this->header['length'] = $length;
        } else {
            throw new IncorrectDimensionsException("The length must be strictly positive: ${length}");
        }
    }

    /**
     * @param int $height
     * @throws IncorrectDimensionsException
     */
    private function set_height(int $height): void
    {
        if ($height > 0) {
            $this->header['height'] = $height;
        } else {
            throw new IncorrectDimensionsException("The height must be strictly positive: ${height}");
        }
    }

    /**
     * @param int $pixelType
     * @throws InvalidPixelType
     */
    private function set_pixel_type(int $pixelType): void
    {
        if ($pixelType >= 0 && $pixelType <= 3) {
            $this->header['pixelType'] = $pixelType;
        } else {
            throw new InvalidPixelType("The pixel type value ${pixelType} is incorrect, it must be between 0 and 3");
        }
    }

    /**
     * @param int $pos
     * @return int
     * @throws IncorrectDimensionsException
     * @throws IncorrectHeaderException
     * @throws InvalidPixelType
     * @throws ChunkOutOfRangeException
     */
    private function read_header(int $pos): int
    {
        if ($this->has_correct_header($pos)) {
            $this->set_length($this->read_chunk2int($pos + 4, 4));
            $this->set_height($this->read_chunk2int($pos + 8, 4));
            $this->set_pixel_type($this->read_chunk2int($pos + 12, 1));
            return $pos + 13;
        }
        throw new IncorrectHeaderException('The header is incorrectly formed');
    }

    /**
     * @return array
     */
    public function getHeader(): array
    {
        return $this->header;
    }

    //=======================================
    // COMMENT FUNCTIONS
    //=======================================

    /**
     * @param string $comment
     */
    private function set_comment(string $comment): void
    {
        $this->comment .= $comment;
    }

    /**
     * @param int $pos
     * @return int
     * @throws ChunkOutOfRangeException
     */
    private function read_comment(int $pos): int
    {
        $length = $this->read_chunk2int($pos, 4);
        $comment = $this->read_chunk($pos + 4, $length);
        $this->set_comment($comment);
        return $pos + 4 + $length;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    //=======================================
    // DATA FUNCTIONS
    //=======================================

    /**
     * @param string $data
     */
    private function set_data(string $data): void
    {
        $this->data .= $data;
    }

    /**
     * @param int $pos
     * @return int
     * @throws ChunkOutOfRangeException
     */
    private function read_data(int $pos): int
    {
        $length = $this->read_chunk2int($pos, 4);
        $this->set_data($this->read_chunk2bin($pos + 4, $length));
        return $pos + 4 + $length;
    }

    //=======================================
    // PALLET FUNCTIONS
    //=======================================

    /**
     * @param string $colorBits
     * @return array
     */
    private function read_color(string $colorBits): array
    {
        $color = [];
        foreach (str_split($colorBits, 8) as $hue) {
            $color[] = bindec($hue);
        }
        return $color;
    }

    /**
     * @param string $palletBits
     */
    private function set_pallet(string $palletBits): void
    {
        $this->pallet = [];
        foreach (str_split($palletBits, 24) as $colorBits) {
            $this->pallet[] = $this->read_color($colorBits);
        }
    }

    /**
     * @param int $pos
     * @return int
     * @throws ChunkOutOfRangeException
     * @throws IncorrectPalletException
     */
    private function read_pallet(int $pos): int
    {
        $length = $this->read_chunk2int($pos, 4);
        if ($length % 3 === 0 && $length <= 256) {
            $this->set_pallet($this->read_chunk2bin($pos + 4, $length));
            return $pos + 4 + $length;
        }

        throw new IncorrectPalletException('The pallet size is invalid');
    }
}
