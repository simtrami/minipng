#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install apt-utils debconf git wget libzip-dev zlib1g-dev zip unzip -yqq
pecl install xdebug

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Here you can install any other extension that you need
docker-php-ext-configure zip --with-libzip
docker-php-ext-install zip
docker-php-ext-enable xdebug

useradd -M user
